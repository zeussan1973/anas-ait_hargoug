<?php 
class Personne{
    public $id;
    public $nom;
    public $prénom;
    public $age;

    function __construct($id,$nom,$prénom,$age)
    {
        $this->id =$id;
        $this->nom =$nom;
        $this->prénom =$prénom;
        $this->age =$age;
    }
    function get_id() {
        return $this->id;
      }
    function get_nom() {
        return $this->nom;
      }
    function get_prénom() {
        return $this->prénom;
      }
    function get_age() {
        return $this->age;
      }
    
}



?>